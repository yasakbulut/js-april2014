var button = document.getElementById('my_button');
button.addEventListener('click', function () {
  var newParagraph = document.createElement("p");
  var newContent = document.createTextNode("Hi there and greetings! It is now " + (new Date()));
  newParagraph.appendChild(newContent);
  var wrapper = document.getElementById("wrapper");
  wrapper.appendChild(newParagraph);
});