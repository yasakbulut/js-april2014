$(document).ready(function () {
  var button = $('#my_button');
  button.click(function () {
    var newParagraph = $('<p></p>');
    newParagraph.text("Hi there and greetings! It is now " + (new Date()));
    var wrapper = $('#wrapper');
    wrapper.append(newParagraph);
  });
});