var button1 = document.getElementById('button1');
var button2 = document.getElementById('button2');
var button3 = document.getElementById('button3');

button1.addEventListener('click', function () {
  var event_red = new CustomEvent('a_very_important_event', {'detail': {'color': 'red', 'time': new Date()}});
  document.dispatchEvent(event_red);
});
button2.addEventListener('click', function () {
  var event_green = new CustomEvent('a_very_important_event', {'detail': {'color': 'green', 'time': new Date()}});
  document.dispatchEvent(event_green);
});
button3.addEventListener('click', function () {
  var event_blue = new CustomEvent('a_very_important_event', {'detail': {'color': 'blue', 'time': new Date()}});
  document.dispatchEvent(event_blue);
});

document.addEventListener('a_very_important_event', function (e) {
  var elem = document.getElementById('output');
  elem.textContent = "We have just received a very important event with color: " + e.detail.color + " and time: " + e.detail.time;
}, false);