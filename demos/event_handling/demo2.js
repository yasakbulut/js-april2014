$(document).ready(function () {
  var button1 = $('#button1');
  var button2 = $('#button2');
  var button3 = $('#button3');
  button1.click(function () {
    var event_red = jQuery.Event("a_very_important_event");
    event_red.detail = {'color': 'red', 'time': new Date()};
    $(document).trigger(event_red)
  });
  button2.click(function () {
    var event_green = jQuery.Event("a_very_important_event");
    event_green.detail = {'color': 'green', 'time': new Date()};
    $(document).trigger(event_green)
  });
  button3.click(function () {
    var event_blue = jQuery.Event("a_very_important_event");
    event_blue.detail = {'color': 'blue', 'time': new Date()};
    $(document).trigger(event_blue)
  });
  $(document).on('a_very_important_event',function(e){
    $('#output').text("We have just received a very important event with color: " + e.detail.color + " and time: " + e.detail.time);

  });
});
